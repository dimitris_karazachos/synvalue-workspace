import { NgModule } from '@angular/core';
import { SynvalueLibComponent } from './synvalue-lib.component';



@NgModule({
  declarations: [SynvalueLibComponent],
  imports: [
  ],
  exports: [SynvalueLibComponent]
})
export class SynvalueLibModule { }
