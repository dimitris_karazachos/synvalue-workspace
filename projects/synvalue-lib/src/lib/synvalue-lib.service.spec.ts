import { TestBed } from '@angular/core/testing';

import { SynvalueLibService } from './synvalue-lib.service';

describe('SynvalueLibService', () => {
  let service: SynvalueLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SynvalueLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
