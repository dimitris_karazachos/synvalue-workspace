import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynvalueLibComponent } from './synvalue-lib.component';

describe('SynvalueLibComponent', () => {
  let component: SynvalueLibComponent;
  let fixture: ComponentFixture<SynvalueLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SynvalueLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynvalueLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
