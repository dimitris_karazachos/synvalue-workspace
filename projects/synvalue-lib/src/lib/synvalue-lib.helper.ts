import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
// tslint:disable-next-line:class-name
export class svlib {

  constructor() { }

  public static isNullOrUndefined(obj: any | Array<any>): boolean {
    if (Array.isArray(obj)) {
      for (const item of obj) {
        if (item !== null || item !== undefined) {
          return false;
        }
      }
      return true;
    }

    return obj === null || obj === undefined;
  }

  public static isNotNullOrUndefined(obj: any | Array<any>): boolean {
    if (Array.isArray(obj)) {
      for (const item of obj) {
        if (item === null || item === undefined) {
          return false;
        }
      }
      return true;
    }

    return obj !== null && obj !== undefined;
  }

  public static isNotEmpty(obj: Array<any>): boolean {
    return this.isNotNullOrUndefined(obj) && obj.length > 0;
  }

  public static isEmptyObject(obj): boolean {
    return JSON.stringify(obj) === '{}';
  }

  public static isNullOrEmpty(inpt: string | Array<any>): boolean {
    if (inpt === null || inpt === undefined) {
      return true;
    }

    if (typeof inpt === 'string') {
      return inpt === '';
    } else {
      return inpt.length === 0;
    }
  }

  public static createDownLoadLink(blob: Blob, filename: string): void {
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    }
    else {
      const url = window.URL.createObjectURL(blob);

      const link = document.createElement('a');
      document.body.appendChild(link);
      link.setAttribute('style', 'display: none');
      link.href = url;
      link.download = filename;
      link.click();

      window.URL.revokeObjectURL(url);
    }
  }

  public static openDownLoadLink(blob: Blob, filename: string): void {
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    }
    else {
      window.open(URL.createObjectURL(blob));
    }
  }

  public static getMapKeys(map: Map<any, any>): Array<any> {
    return Array.from(map.keys());
  }

  public static compareObjects(base: any, compare: any): boolean {

    for (const baseProperty in base) {
      if (compare.hasOwnProperty(baseProperty)) {
        switch (typeof base[baseProperty]) {
          case 'object':
            if (typeof compare[baseProperty] !== 'object' || !this.compareObjects(base[baseProperty], compare[baseProperty])) {
              return false;
            }
            break;
          case 'function':
            if (typeof compare[baseProperty] !== 'function' || base[baseProperty].toString() !== compare[baseProperty].toString()) {
              return false;
            }
            break;
          default:
            if (base[baseProperty] !== compare[baseProperty]) {
              return false;
            }
        }
      } else {
        return false;
      }
    }

    return true;
  }

  public static convertToBoolean(input: string): boolean | undefined {
    try {
      return JSON.parse(input);
    }
    catch (e) {
      return undefined;
    }
  }

  public static dateFromGreekFormat(inpt: string): Date {
    try {
      const dateParts = inpt.split('/');
      return new Date(Date.parse(`${dateParts[2]}-${dateParts[1]}-${dateParts[0]}`));
    } catch {
      console.error(`Cannot convert "${inpt}" into date type`);
      return null;
    }
  }
}
